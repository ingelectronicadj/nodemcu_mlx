#include <Arduino.h>
#include <stdio.h>
#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <Adafruit_MLX90614.h>
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#define BLYNK_PRINT Serial
#define MAX_TEMP 27
#define MIN_TEMP 19

WidgetLCD lcd(V3);
char auth[] = "959eff4769654b13938a49a62df5545b";
char ssid[] = "PACO";
char pass[] = "9404POKEMON";
WidgetLED ledAlertaMax(V1);
WidgetLED ledAlertaMin(V2);
BlynkTimer timer;

#define   CS_PIN1 D8
File archivo;

Adafruit_MLX90614 mlx_1 = Adafruit_MLX90614(); // default address is 0x5A
float temp_amb, temp_obj = 0.0;

void writeSD(float temp1, float temp2);
void ConfigSD(int SD_CS);

void ConfigSD(int SD_CS) {
  printf("Configurando modulo SD\n");
  if(!SD.begin(SD_CS)){
    printf("no inicio la SD");
    //no hace nada mas hasta que se conecte la SD
    while(1);
  }
  printf("Encuentra tarjeta SD\n");
  //abre el archivo y lo cierra de inmediato.
  printf("creando data.txt...\n");
  archivo = SD.open("data.txt",FILE_WRITE);
  archivo.close();
  // Verifica si el archivo
  if (SD.exists("data.txt")) {
    printf("El archivo data.txt existe\n");
  } else {
    printf("El archivo data.txt no existe\n");
  }
}

float averageRead(float makeRead()) {
    int numberOfReadings = 10;
    float average = 0.0;
    for (int x = 0; x < numberOfReadings; x++){
        average += makeRead()/numberOfReadings;
    }
    return average;
}

void myTimerEvent(){
  Blynk.virtualWrite(V5,temp_amb);
  Blynk.virtualWrite(V6,temp_obj);
  
  if(temp_obj>=MAX_TEMP){
    ledAlertaMax.on();
    Serial.println("Alerta - fuera de rango maximo");
  }
  else if(temp_obj<=MIN_TEMP){
    ledAlertaMin.on();
    Serial.println("Alerta - fuera de rango minimo");
  }
  else {
    ledAlertaMax.off();
    ledAlertaMin.off();
    Serial.println("En buenas condiciones de temperatura");
  }
}

void setup() {
  delay(2000);// pequeña demora para ver que pasa al inicio
  Serial.begin(9600);
  ConfigSD(CS_PIN1);
  Wire.begin(D1, D2); // Bus I2C del NodeMCU SDA=D1, SCL=D2 
  mlx_1.begin();
  Blynk.begin(auth, ssid, pass);
  timer.setInterval(2000L, myTimerEvent);
  Blynk.virtualWrite(V1,0);
  Blynk.virtualWrite(V2,0);
  lcd.clear(); //Use it to clear the LCD Widget
}

void writeSD(float temp1, float temp2) {
  archivo = SD.open("data.txt",FILE_WRITE);
  //si el archivo es viable se escribe.
  if(archivo)
  {
    archivo.print("Temperatura Ambiente: ");
    archivo.println(temp1);
    archivo.print("Temperatura Objeto: ");
    archivo.println(temp2);
    archivo.close();
  } else {
    printf("no pudo abrir el archivo");
  }
  delay(200);
}

float ReadObjTempC(){return (float)mlx_1.readObjectTempC();}
float ReadAmbTempC(){return (float)mlx_1.readAmbientTempC();}

void loop() {
  printf("Lectura SMBus/I2C\nSensor_1 MLX90614\n");
  printf("----------------------------------------------------\n");
  temp_obj = averageRead(ReadObjTempC);
  temp_amb = averageRead(ReadAmbTempC);
  printf("Promedio de 10 muestras\nTemp_Ambiente = %.2fºC\tTemp_Objeto = %.2fºC\n",temp_amb, temp_obj);
  printf("----------------------------------------------------\n");
  
  writeSD(temp_amb, temp_obj);
  Blynk.run();
  timer.run();
}



